const vita = [
  {
    title: "VITA Easyshade® V",
    shortDescription: 'El espectrofotómetro VITA Easyshade V ha sido desarrollado para la determinación del color precisa, rápida y fiable de dientes naturales, así como de restauraciones cerámicas. Determinación precisa y reproducible del color dental en segundos gracias a la más avanzada tecnología de medición. Resultados de medición objetivos y fiables mediante la tecnología LED independiente del entorno. Proceso seguro y rentable gracias a la información exacta sobre el color dental en los estándares de colores VITA, que posibilita una reproducción cromática segura y, por consiguiente, la reducción de correcciones del color. Utilización sencilla e intuitiva gracias a la pantalla táctil con software autoexplicativo. Comunicación digital eficiente para el intercambio de imágenes e información sobre color dental entre la clínica y el laboratorio.',
    imageUrl: 'assets/images/vita/1.jpg',
    idModal: 1,
    modalImageUrl: "assets/images/modalImages/vita/1.jpg"
  },
  {
    title: "VITAPAN EXCELL® Anterior",
    shortDescription: 'Dientes anteriores de gama alta de composite MRP, ricos en matices, corpóreos y naturales. Confección sencilla de prótesis altamente estéticas gracias a formas dentales naturales con proporciones “áureas” Extraordinario juego de colores y luces gracias a la excelente dinámica lumínica y a la transición cromática armoniosa. Buena reproducción cromática en caso de dientes remanentes naturales gracias a la máxima fidelidad cromática a la guía de colores, excelente estabilidad gracias a la elevada capacidad de carga y a la resistencia a la abrasión Configuración excelente de la encía gracias al diseño ancho del cuello dental',
    imageUrl: 'assets/images/vita/2.jpg',
    idModal: 2,
    modalImageUrl: "assets/images/modalImages/vita/2.jpg"
  },

  {
    title: "VITAPAN PLUS® Anterior",
    shortDescription: 'Dientes anteriores de gama alta y apariencia natural, de composite MRP con un plus de translucidez. Juego de luces natural gracias a la extraordinaria translucidez derivada de la elevada proporción de esmalte Facilidad de reconstrucción de la estética juvenil gracias a las formas dentales corpóreas, juveniles y vitales Excelente estabilidad gracias a la elevada capacidad de carga y a la resistencia a la abrasión Desbastado y pulido eficientes gracias a la homogeneidad óptima del material y a la estabilidad de los ángulos',
    imageUrl: 'assets/images/vita/3.jpg',
    idModal: 3,
    modalImageUrl: "assets/images/modalImages/vita/3.jpg"
  },
  {
    title: "VITAPAN® LINGOFORM Posterior",
    shortDescription: 'Dientes posteriores de gama alta, corpóreos y multifuncionales, de composite MRP. Montaje sencillo e intuitivo gracias a la determinación “automática” de la céntrica, derivada del diseño de la superficie masticatoria conforme al principio del engranaje De uso universal y fiable para todos los conceptos protésicos gracias al diseño multifuncional de las superficies masticatorias rapidez de montaje sin necesidad de desbastado laborioso, gracias a la céntrica exactamente definida Excelente estabilidad gracias a la elevada capacidad de carga, la resistencia a la abrasión y la función apoyada en la céntrica Gran facilidad de mecanización gracias a la homogeneidad óptima del material y a la estabilidad de los ángulos',
    imageUrl: 'assets/images/vita/4.jpg',
    idModal: 4,
    modalImageUrl: "assets/images/modalImages/vita/4.jpg"
  },
  {
    title: "VITA PHYSIODENS® Anterior",
    shortDescription: 'Dientes anteriores de gama alta de composite MRP, corpóreos y fieles al detalle. Integración óptima en la dentadura natural remanente gracias a las formas complejas y fieles al detalle Juego de luces natural gracias a la textura superficial variada Buena reproducción cromática en caso de dientes remanentes naturales gracias a la gran fidelidad cromática a la guía de colores y a la amplia selección de colores Enmascaramiento fiable de estructuras secundarias y terciarias gracias a un elevado volumen dental (basal) Excelente estabilidad gracias a la elevada capacidad de carga y a la resistencia a la abrasión',
    imageUrl: 'assets/images/vita/5.jpg',
    idModal: 5,
    modalImageUrl: "assets/images/modalImages/vita/5.jpg"
  },
  {
    title: "VITA PHYSIODENS® Posterior",
    shortDescription: 'Dientes posteriores de gama alta de composite MRP, corpóreos y fieles al detalle. Restablecimiento sencillo de la función masticatoria gracias a la morfología natural de las superficies oclusales Determinación intuitiva de la céntrica y buena intercuspidación gracias al diseño oclusal apoyado en la céntrica Buena reproducción cromática en caso de dientes remanentes naturales gracias a la fidelidad cromática a la guía de colores y a la amplia selección de colores Enmascaramiento fiable de estructuras terciarias gracias a un elevado volumen dental (basal) Excelente estabilidad gracias a la elevada capacidad de carga y a la resistencia a la abrasión',
    imageUrl: 'assets/images/vita/6.jpg',
    idModal: 6,
    modalImageUrl: "assets/images/modalImages/vita/6.jpg"
  },
  {
    title: "VITA LUMIN® VACUUM Anterior",
    shortDescription: 'Los dientes de cerámica son ideales para todas las indicaciones en las prótesis parciales y completas de alta gama. Poseen un brillo elevado gracias a su efecto Lumin inherente, e imitan a la naturaleza en cuanto a su conductividad de la luz, opalescencia y fluorescencia. Las piezas únicas confeccionadas a mano en Alemania incorporan elementos de anclaje mecánicos adicionales en la base de la prótesis, a fin de garantizar la estabilidad de las prótesis a largo plazo: Dientes anteriores con pernos recubiertos de oro insertados en el núcleo de cerámica dura. Dientes posteriores con cavidades retentivas.',
    imageUrl: 'assets/images/vita/7.jpg',
    idModal: 7,
    modalImageUrl: "assets/images/modalImages/vita/7.jpg"
  },
  {
    title: "VITA LUMIN® VACUUM Posterior",
    shortDescription: 'Los dientes de cerámica son ideales para todas las indicaciones en las prótesis parciales y completas de alta gama. Poseen un brillo elevado gracias a su efecto Lumin inherente, e imitan a la naturaleza en cuanto a su conductividad de la luz, opalescencia y fluorescencia. Las piezas únicas confeccionadas a mano en Alemania incorporan elementos de anclaje mecánicos adicionales en la base de la prótesis, a fin de garantizar la estabilidad de las prótesis a largo plazo: Dientes anteriores con pernos recubiertos de oro insertados en el núcleo de cerámica dura. Dientes posteriores con cavidades retentivas.',
    imageUrl: 'assets/images/vita/8.jpg',
    idModal: 8,
    modalImageUrl: "assets/images/modalImages/vita/8.jpg"
  },
  {
    title: "VITA MFT® Anterior",
    shortDescription: 'Dientes anteriores básicos de material de polímero HC para la prótesis estándar sólida. Confección fiable de prótesis estéticas sólidas gracias al diseño incisal y a las características angulares fieles al modelo natural. Reproducción sencilla de un juego de luces natural gracias a la proporción esmalte-dentina equilibrada, a los mamelones integrados y a la textura. Reproducción cromática fiable gracias a la buena fidelidad cromática al estándar de colores VITA (VITA classical A1–D4®).',
    imageUrl: 'assets/images/vita/9.jpg',
    idModal: 9,
    modalImageUrl: "assets/images/modalImages/vita/9.jpg"
  },
  {
    title: "VITA MFT® Posterior",
    shortDescription: 'Dientes posteriores básicos de polímero HC con optimización funcional de las superficies masticatorias. Montaje de la prótesis simplificado mediante puntos de contacto predefinidos para una intercuspidación automática. Elevada seguridad de montaje gracias a la optimización funcional de las superficies masticatorias para lograr una guía céntrica dirigida. Menor trabajo de tallado selectivo gracias al diseño basal optimizado. Uso fiable para todos los conceptos protésicos gracias al diseño multifuncional de las superficies masticatorias.',
    imageUrl: 'assets/images/vita/10.jpg',
    idModal: 10,
    modalImageUrl: "assets/images/modalImages/vita/10.jpg"
  },
  {
    title: "VITACOLL® Agente adhesivo",
    shortDescription: 'VITACOLL es el agente adhesivo seguro para unir dientes de resina y materiales base. Debido al empleo de los más diversos materiales base, a menudo es difícil saber si estos materiales se van a unir de forma segura a los dientes de resina disponibles. Utilizar VITACOLL garantiza la seguridad necesaria. Frasco de 100 ml. El agente adhesivo VITACOLL proporciona una unión química segura de los dientes de resina con materiales termopolimerizables y autopolimerizables, así como con materiales de inyección de polimetacrilato, evitando que los dientes se separen de la base.',
    imageUrl: 'assets/images/vita/11.jpg',
    idModal: 11,
    modalImageUrl: "assets/images/modalImages/vita/11.jpg"
  },
  {
    title: "Película de aislamiento de silicona VITAFOL® H",
    shortDescription: 'VITAFOL H es un material aislante de silicona para la protección de dientes de resina y cerámica durante la confección de la prótesis. VITAFOL H se utiliza para proteger dientes de resina y cerámica y permite conservar las modelaciones más finas de la cresta gingival; las zonas cubiertas con VITAFOL H se mantienen limpias y no requieren un acabado posterior. Tampoco hace falta aislarlas en dirección al yeso. De este modo es imposible que el aislamiento llegue a las superficies basales de los dientes. En los dientes de resina se consigue evitar así la formación de una capa de separación entre los dientes y el material base. El efecto tampón de VITAFOL H elimina los daños que pueden producirse durante el prensado y la retirada del revestimiento.',
    imageUrl: 'assets/images/vita/12.jpg',
    idModal: 12,
    modalImageUrl: "assets/images/modalImages/vita/12.jpg"
  },
  {
    title: "VITA ENAMIC®",
    shortDescription: 'La única cerámica híbrida dental del mundo con matriz dual de cerámica y polímero. Disponible en variantes monocromáticas y multicromáticas en hasta tres grados de translucidez (T, HT, ST). Confección de restauraciones resistentes gracias a la ceramica hibrida con gran capacidad de carga y propiedades de absorcion de las fuerzas oclusales. Tratamiento no invasivo o mínimamente invasivo, ya que la ceramica hibrida elastica posibilita grosores de pared reducidos. Confección de construcciones delgadas, fieles al detalle y de ajuste preciso, gracias al material compuesto con elasticidad integrada. Reconstrucciones rentables gracias a la rapida confeccion CAM y al acabado eficiente mediante pulido sin coccion.',
    imageUrl: 'assets/images/vita/13.jpg',
    idModal: 13,
    modalImageUrl: "assets/images/modalImages/vita/13.jpg"
  },
  {
    title: "VITA ENAMIC® multiColor",
    shortDescription: 'VITA ENAMIC multiColor es la pieza en bruto de cerámica híbrida para la reproducción de la transición cromática natural con solo pulsar un botón. Bloque para CAD/CAM que dispone de una suave transición cromática integrada desde la zona cervical a la incisal. Confección de restauraciones resistentes gracias a la ceramica hibrida con gran capacidad de carga y propiedades de absorcion de las fuerzas oclusales. Tratamiento no invasivo o mínimamente invasivo, ya que la ceramica hibrida elastica posibilita grosores de pared reducidos. Confección de construcciones delgadas, fieles al detalle y de ajuste preciso, gracias al material compuesto con elasticidad integrada. Reconstrucciones rentables gracias a la rapida confeccion CAM y al acabado eficiente mediante pulido sin coccion.',
    imageUrl: 'assets/images/vita/14.jpg',
    idModal: 14,
    modalImageUrl: "assets/images/modalImages/vita/14.jpg"
  },
  {
    title: "VITABLOCS® Mark II",
    shortDescription: 'VITABLOCS Mark II: piezas en bruto monocromas de cerámica de feldespato de color dental, que se integran cromáticamente de forma óptima en la sustancia dental remanente. Confección de restauraciones fiables gracias al material resistente perfectamente acreditado en el ambito clinico. Restauraciones altamente estéticas gracias a un material con un excelente juego cromatico y luminico. Reconstrucciones rentables gracias a la rapida confeccion CAM y al acabado eficiente mediante pulido.',
    imageUrl: 'assets/images/vita/15.jpg',
    idModal: 15,
    modalImageUrl: "assets/images/modalImages/vita/15.jpg"
  },
  {
    title: "VITABLOCS® TriLuxe forte",
    shortDescription: 'VITABLOCS TriLuxe forte: piezas en bruto policromas de cerámica de feldespato de color dental con transición cromática integrada, para la reproducción del juego cromático natural. Confección de restauraciones fiables gracias al material resistente perfectamente acreditado en el ambito clinico. Restauraciones altamente estéticas gracias a un material con un excelente juego cromatico y luminico. Reconstrucciones rentables gracias a la rapida confeccion CAM y al acabado eficiente mediante pulido.',
    imageUrl: 'assets/images/vita/16.jpg',
    idModal: 16,
    modalImageUrl: "assets/images/modalImages/vita/16.jpg"
  },
  {
    title: "VITABLOCS RealLife®",
    shortDescription: 'VITABLOCS RealLife: piezas en bruto policromas de cerámica de feldespato de color dental con estructura de capas 3D integrada, para la reproducción de la transición cromática natural en la zona de los dientes anteriores. Confección de restauraciones fiables gracias al material resistente perfectamente acreditado en el ambito clinico. Restauraciones altamente estéticas gracias a un material con un excelente juego cromatico y luminico. Reconstrucciones rentables gracias a la rapida confeccion CAM y al acabado eficiente mediante pulido.',
    imageUrl: 'assets/images/vita/17.jpg',
    idModal: 17,
    modalImageUrl: "assets/images/modalImages/vita/17.jpg"
  },
  {
    title: "VITA SUPRINITY® PC",
    shortDescription: 'Generación de cerámica vítrea altamente resistente, reforzada con dióxido de circonio Disponible como piezas en bruto monocromas en dos grados de translucidez (T, HT) Elevada seguridad gracias a la matriz de la cerámica vítrea reforzada con dióxido de circonio. Resultados de gran precisión gracias a las piezas en bruto de material con estabilidad de ángulos. Facilidad de manipulación gracias a las buenas propiedades de pulido y a la elevada estabilidad de cocción. Estética excelente gracias a la translucidez, la opalescencia y la fluorescencia integradas.',
    imageUrl: 'assets/images/vita/18.jpg',
    idModal: 18,
    modalImageUrl: "assets/images/modalImages/vita/18.jpg"
  },
  {
    title: "VITA YZ® SOLUTIONS",
    shortDescription: 'La solución de sistema para unas reconstrucciones en dióxido de circonio precisas, eficientes y fieles a la guía de colores VITA YZ® SOLUTIONS permite la confección precisa y fiel a la guía de colores de prótesis monolíticas y total o parcialmente recubiertas.',
    imageUrl: 'assets/images/vita/19.jpg',
    idModal: 19,
    modalImageUrl: "assets/images/modalImages/vita/19.jpg"
  },
  {
    title: "VITA YZ® XT",
    shortDescription: 'VITA YZ XT son piezas en bruto de ZrO2 extratranslúcidas para la confección de prótesis de dientes anteriores monolíticas y parcialmente recubiertas. Las piezas en bruto VITA YZ XT están disponibles en múltiples variantes: VITA YZ XTWhite (sin colorear), VITA YZ XTColor (monocromo, de color dental), VITA YZ XTMulticolor (policromo, de color dental). Reproducción fiable de los colores dentalesgracias a un sistema de materiales con componentes perfectamente armonizadosentre sí. Confección de prótesis con precisión de ajustemediante piezas en bruto de material con estabilidad de ángulos, una microestructura homogénea y propiedades de sinterización probadas. Obtención eficiente de resultados estéticos sólidos gracias a las piezas en bruto (Multi)Color precoloreadas de color dental con gran fidelidad a la guía de colores VITA classical A1–D4.',
    imageUrl: 'assets/images/vita/20.jpg',
    idModal: 20,
    modalImageUrl: "assets/images/modalImages/vita/20.jpg"
  },
  {
    title: "VITA YZ® ST",
    shortDescription: 'VITA YZ ST son piezas en bruto de ZrO2 supertranslúcidas para la confección de prótesis de dientes posteriores monolíticas y parcialmente recubiertas. Las piezas en bruto VITA YZ ST están disponibles en múltiples variantes: VITA YZ STWhite (sin colorear), VITA YZ STColor (monocromo, de color dental), VITA YZ STMulticolor (policromo, de color dental). Reproducción fiable de los colores dentalesgracias a un sistema de materiales con componentes perfectamente armonizadosentre sí. Confección de prótesis con precisión de ajustemediante piezas en bruto de material con estabilidad de ángulos, una microestructura homogénea y propiedades de sinterización probadas. Obtención eficiente de resultados estéticos sólidos gracias a las piezas en bruto (Multi)Color precoloreadas de color dental con gran fidelidad a la guía de colores VITA classical A1–D4.',
    imageUrl: 'assets/images/vita/21.jpg',
    idModal: 21,
    modalImageUrl: "assets/images/modalImages/vita/21.jpg"
  },
  {
    title: "VITA YZ® HT",
    shortDescription: 'VITA YZ HT son piezas en bruto de ZrO2 altamente translúcidas para la confección de estructuras completamente recubiertas personalizadas y con estética natural. Las piezas en bruto VITA YZ están disponibles en múltiples variantes: VITA YZ HTWhite (sin colorear), VITA YZ HTColor (monocromo, de color dental). Reproducción fiable de los colores dentalesgracias a un sistema de materiales con componentes perfectamente armonizadosentre sí. Confección de prótesis con precisión de ajustemediante piezas en bruto de material con estabilidad de ángulos, una microestructura homogénea y propiedades de sinterización probadas. Obtención eficiente de resultados estéticos sólidos gracias a las piezas en bruto Color precoloreadas de color dental con gran fidelidad a la guía de colores VITA classical A1–D4.',
    imageUrl: 'assets/images/vita/22.jpg',
    idModal: 22,
    modalImageUrl: "assets/images/modalImages/vita/22.jpg"
  },
  {
    title: "VITA YZ® T",
    shortDescription: 'VITA YZ T son piezas en bruto de ZrO2 translúcidas para la confección de estructuras completamente recubiertas personalizadas y con estética natural. Las piezas en bruto VITA YZ T están disponibles en múltiples variantes: VITA YZ TWhite (sin colorear), VITA YZ TColor (monocromo, de color dental). Reproducción fiable de los colores dentalesgracias a un sistema de materiales con componentes perfectamente armonizadosentre sí. Confección de prótesis con precisión de ajustemediante piezas en bruto de material con estabilidad de ángulos, una microestructura homogénea y propiedades de sinterización probadas. Obtención eficiente de resultados estéticos sólidos gracias a las piezas en bruto Color precoloreadas de color dental con gran fidelidad a la guía de colores VITA classical A1–D4.',
    imageUrl: 'assets/images/vita/23.jpg',
    idModal: 23,
    modalImageUrl: "assets/images/modalImages/vita/23.jpg"
  },
  {
    title: "VITA Rapid Layer Technology",
    shortDescription: 'Técnica digital para la confección de puentes de unión completamente anatómicos con una estructura de soporte de dióxido de circonio y una estructura de recubrimiento de cerámica de feldespato/híbrida. Ahorro de tiempo y costes gracias al proceso de confección digital de las estructuras de soporte y de recubrimiento. Elevada seguridad y riesgo mínimo de astillamiento gracias a la estructura de recubrimiento monolítica. Unión sencilla y segura de las estructuras de soporte y de recubrimiento gracias a la unión adhesiva.',
    imageUrl: 'assets/images/vita/24.jpg',
    idModal: 24,
    modalImageUrl: "assets/images/modalImages/vita/24.jpg"
  },
  {
    title: "VITA CAD-Temp® monoColor",
    shortDescription: 'Composite de polímero de acrilato altamente reticulado con microrrelleno para la confección de restauraciones provisionales de larga duración. Buena estabilidad a largo plazo gracias a la buena estabilidad superficial y resistencia a la rotura (periodo de uso de hasta 3 años). Efecto cromático natural gracias a sus buenas propiedades fotoópticas. Confección rentable de prótesis provisionales gracias a la rápida confección CAD/CAM.',
    imageUrl: 'assets/images/vita/25.jpg',
    idModal: 25,
    modalImageUrl: "assets/images/modalImages/vita/25.jpg"
  },
  {
    title: "VITA CAD-Temp® multiColor",
    shortDescription: 'Composite de polímero de acrilato altamente reticulado con microrrelleno para la confección de restauraciones provisionales de larga duración. Buena estabilidad a largo plazo gracias a la buena estabilidad superficial y resistencia a la rotura (periodo de uso de hasta 3 años). Efecto cromático natural gracias a sus buenas propiedades fotoópticas.Confección rentable de prótesis provisionales gracias a la rápida confección CAD/CAM.',
    imageUrl: 'assets/images/vita/26.jpg',
    idModal: 26,
    modalImageUrl: "assets/images/modalImages/vita/26.jpg"
  },
  {
    title: "VITA CAD-Waxx",
    shortDescription: 'Polímero de acrilato calcinable sin dejar residuos, para la confección asistida por ordenador de encerados, así como para la confección de guías quirúrgicas. Reconstrucciones rentables gracias al modelado asistido por ordenador. Diseño y ajuste exactos gracias a la elevada resistencia a la torsión y exactitud dimensional. Resultados de colado sin rechupes y de prensado sin poros gracias al material CAD-Waxx calcinable sin dejar residuos.',
    imageUrl: 'assets/images/vita/27.jpg',
    idModal: 27,
    modalImageUrl: "assets/images/modalImages/vita/27.jpg"
  },
  {
    title: "VITA ENAMIC® IS",
    shortDescription: 'Piezas en bruto de cerámica híbrida, que incorporan una interfase integrada con una base adhesiva/de titanio Disponible como cerámica híbrida VITA ENAMIC IS (T/HT) para reconstrucciones definitivas. Confección de restauraciones implantosoportadas resistentes gracias a la cerámica híbrida con gran capacidad de carga. Reconstrucciones rentables gracias a la rápida confección CAM y al acabado eficiente.',
    imageUrl: 'assets/images/vita/28.jpg',
    idModal: 28,
    modalImageUrl: "assets/images/modalImages/vita/28.jpg"
  },
  {
    title: "VITA CAD-Temp® IS",
    shortDescription: 'Piezas en bruto de composite, que incorporan una interfase integrada con una base adhesiva/de titanio. Disponible como composite VITA CAD-Temp IS para restauraciones provisionales. Confección eficiente de prótesis provisionales gracias al proceso íntegramente digital. Efecto cromático natural gracias a sus buenas propiedades fotoópticas.',
    imageUrl: 'assets/images/vita/29.jpg',
    idModal: 29,
    modalImageUrl: "assets/images/modalImages/vita/29.jpg"
  },
  {
    title: "VITA Powder Scan Spray",
    shortDescription: 'El VITA Powder Scan Spray es una suspensión de pigmentos libre de dióxido de ti-tanio pulverizable, de color azul y con aroma a menta. Está indicado para la aplica-ción intraoral (superficie dental) y extraoral (muñón / modelo) para realizar la impre-sión optoe-lectrónica en restauraciones CAD/CAM. El VITA Powder Scan Sprayproporciona un recubrimiento mate a la superficie del diente o del modelo, permiti-endo así un registro óptimo con la cámara intraoral (p. ej., con Sirona CEREC 3 o CE-REC AC) o con el escáner.',
    imageUrl: 'assets/images/vita/30.jpg',
    idModal: 30,
    modalImageUrl: "assets/images/modalImages/vita/30.jpg"
  },
  {
    title: "VITA Firing Paste",
    shortDescription: 'VITA Firing Paste: Material refractario, listo para usar, para la confección sencilla y rápida de soportes de cocción individuales. Gracias a su consistencia suave y cremosa posee propiedades de elaboración extraordinarias. Los inlays, onlays, carillas y coronas pueden fijarse fácilmente en pernos de platino o directamente en el soporte de cocción o en la fibra de vidrio.Tras el proceso de cocción, el material puede retirarse fácilmente del objeto de cocción.',
    imageUrl: 'assets/images/vita/31.jpg',
    idModal: 31,
    modalImageUrl: "assets/images/modalImages/vita/31.jpg"
  },
  {
    title: "Rotating Instruments",
    shortDescription: 'La guía de colores VITABLOCS Guide 3D MASTER, con los dientes de muestra de color realizados en cerámica original VITABLOCS Mark II, permite seleccionar los VITABLOCS adecuados de forma sencilla, rápida y exacta.',
    imageUrl: 'assets/images/vita/32.jpg',
    idModal: 32,
    modalImageUrl: "assets/images/modalImages/vita/32.jpg"
  },
  {
    title: "VITA ADIVA® IA-CEM",
    shortDescription: 'VITA ADIVA IA-CEM es un composite de fijación ultraopaco con efecto enmascarador intenso y de fraguado dual para prótesis implantosoportadas. Se utiliza para la fijación adhesiva definitiva de coronas (sobre pilar) y mesoestructuras a bases adhesivas de titanio y a pilares individuales de titanio y de dióxido de circonio. El composite de fijación se complementa con agentes adhesivos adaptados específicamente a cada material, para obtener una unión resistente entre la restauración y el pilar.',
    imageUrl: 'assets/images/vita/33.jpg',
    idModal: 33,
    modalImageUrl: "assets/images/modalImages/vita/33.jpg"
  },
  {
    title: "VITA Modulbox",
    shortDescription: 'VITA MODULBOX es un sistema modular de tamaño A3 para el almacenamiento a largo plazo, de forma clara y adecuada para el material de los productos VITA en laboratorios y consultorios dentales.',
    imageUrl: 'assets/images/vita/34.jpg',
    idModal: 34,
    modalImageUrl: "assets/images/modalImages/vita/34.jpg"
  },
  {
    title: "VITA YZ® XT SHADE LIQUIDS",
    shortDescription: 'Líquidos para la coloración manual de restauraciones monolíticas de VITA YZ XT White antes del proceso de sinterización. Los VITA YZ XT SHADE LIQUIDS están disponibles en todos los colores VITA classical A1–D4 y se pueden caracterizar adicionalmente mediante los VITA YZ EFFECT LIQUIDS.',
    imageUrl: 'assets/images/vita/35.jpg',
    idModal: 35,
    modalImageUrl: "assets/images/modalImages/vita/35.jpg"
  },
  {
    title: "VITA YZ® ST SHADE LIQUIDS",
    shortDescription: 'Líquidos para la coloración individual de restauraciones monolíticas de VITA YZ ST White antes del proceso de sinterización. Además de los 16 colores VITA classical A1–D4, los VITA YZ EFFECT LIQUIDS también están disponibles como colores adicionales para la caracterización personalizada.',
    imageUrl: 'assets/images/vita/36.jpg',
    idModal: 36,
    modalImageUrl: "assets/images/modalImages/vita/36.jpg"
  },
  {
    title: "VITA YZ® HT SHADE LIQUIDS",
    shortDescription: 'Líquidos para la coloración individual de restauraciones monolíticas de VITA YZ HT antes del proceso de sinterización. Los VITA YZ HT SHADE LIQUID están disponibles en siete colores VITA classical A1 – D4 y en 7 colores VITA SYSTEM 3D-MASTER VITA YZ EFFECT LIQUIDS para la zona gingival, cervical e incisal.',
    imageUrl: 'assets/images/vita/37.jpg',
    idModal: 37,
    modalImageUrl: "assets/images/modalImages/vita/37.jpg"
  },
  {
    title: "VITA YZ® EFFECT LIQUID",
    shortDescription: 'Líquidos de efectos para la caracterización personalizada de restauraciones coloreados anteriormente con VITA YZ SHADE LIQUIDS de VITA YZ HT White, VITA YZ ST White y VITA YZ XT White, disponibles en 11 colores. Mediante Blue y Grey se pueden personalizar tanto zonas incisales como de cúspides. Orange y Brown se pueden utilizar para la configuración de fosas o en las zonas interdental y del borde de la corona. Las tres tonalidades Light Pink, Pink y Dark Pink permiten mejorar la armonización cromática de las transiciones hacia la encía.',
    imageUrl: 'assets/images/vita/38.jpg',
    idModal: 38,
    modalImageUrl: "assets/images/modalImages/vita/38.jpg"
  },
  {
    title: "VITA YZ® T COLORING LIQUIDS",
    shortDescription: 'Líquido para la coloración total o parcial de estructuras fresadas de VITA YZ T antes del proceso de sinterización. VITA YZ T COLORING LIQUID está disponible en 4 colores diferentes y está ajustado de tal forma que permite reproducir todos los colores del VITA SYSTEM 3D-MASTER y VITA classical A1–D4 con la cerámica de recubrimiento VITA VM 9.',
    imageUrl: 'assets/images/vita/39.jpg',
    idModal: 39,
    modalImageUrl: "assets/images/modalImages/vita/39.jpg"
  },
  {
    title: "Dientes de resina LINGOFORM",
    shortDescription: 'VITA PM 9 (Pressable Material) es una cerámica prensada ""todo en uno"" basada en la cerámica de recubrimiento de estructura fina VITA VM 9, acreditada en millones de casos clínicos. Es adecuada tanto para la técnica de prensado sin estructura (inlays/onlays, carillas y coronas) como para el sobreprensado de estructuras de dióxido de circonio. Confección racional: el amplio espectro de indicaciones (técnica de prensado sin estructura y técnica de sobreprensado) de VITA PM 9 permite confeccionar de forma rentable y segura las más diversas restauraciones, utilizando un programa de prensado idéntico y manteniendo al mismo tiempo un almacenamiento eficiente.',
    imageUrl: 'assets/images/vita/40.jpg',
    idModal: 40,
    modalImageUrl: "assets/images/modalImages/vita/40.jpg"
  },
  {
    title: "Accesorios para Cerámica prensada",
    shortDescription: 'Émbolos VITA PM desechables Envase con 50 émbolos desechables de 12 mm de diámetro, para pastillas de 2 g. Indicados para todas las cerámicas de prensado. Los émbolos desechables ahorran el laborioso arenado de los émbolos de óxido de aluminio y, gracias a su extraordinaria composición, evitan la formación de microgrietas en el cono de prensado. Sistema de revestimiento VITA PM, 200 g Envase con anillo de mufla, base de mufla y plantilla de mufla. Gracias a sus marcas de sectores interiores bien visibles, el anillo de mufla, fabricado de silicona, permite la localización rápida y selectiva del objeto de prensado, con lo que ahorra tiempo y material de arenado. Líquido de mezcla para material de revestimiento VITA PM. Líquido de mezcla especial para el material de revestimiento VITA PM. Material de revestimiento VITA PM. Material de revestimiento fosfatado y libre de grafito para el calentamiento rápido, especialmente para la cerámica de prensado VITA PM9.',
    imageUrl: 'assets/images/vita/41.jpg',
    idModal: 41,
    modalImageUrl: "assets/images/modalImages/vita/41.jpg"
  },
  {
    title: "VITAVM®13",
    shortDescription: 'VITA VM 13 es una cerámica de feldespato de estructura fina altamente estética perfectamente adaptada al CET de las aleaciones de cocción clásicas (aprox. 13,8 – 15,2 µm/mK). Con total independencia de si las aleaciones clásicas se cuelan, se fresan o se sinterizan.',
    imageUrl: 'assets/images/vita/42.jpg',
    idModal: 42,
    modalImageUrl: "assets/images/modalImages/vita/42.jpg"
  },
  {
    title: "VITA VMK Master®",
    shortDescription: 'VITA VMK Master es una cerámica de recubrimiento de feldespato natural para aleaciones de cocción clásicas (aprox. 13,8 – 15,2 µm/mK). Con total independencia de si las aleaciones clásicas se cuelan, se fresan o se sinterizan. Manipulación precisa y segura. Magistral en cuanto a fiabilidad, aptitud para el uso diario y estética: así es VITA VMK Master.',
    imageUrl: 'assets/images/vita/43.jpg',
    idModal: 43,
    modalImageUrl: "assets/images/modalImages/vita/43.jpg"
  },
  {
    title: "VITA TITANKERAMIK",
    shortDescription: 'El coeficiente de dilatación térmica y las demás propiedades físicas de la VITA TITANKERAMIK se han ajustado exactamente al material titanio. La aplicación correcta de esta cerámica garantiza una unión segura entre la estructura de titanio y la VITA TITANKERAMIK. La óptima armonización de los polvos permite al protésico técnico dental obtener colores luminosos y brillantes de diferente translucidez. La VITA TITANKERAMIK ofrece todas las posibilidades para conseguir buenos resultados estéticos.',
    imageUrl: 'assets/images/vita/44.jpg',
    idModal: 44,
    modalImageUrl: "assets/images/modalImages/vita/44.jpg"
  },
  {
    title: "VITAVM®15",
    shortDescription: 'VITA VM 15 se ha desarrollado como cerámica especial de bajo punto de fusión para el recubrimiento de aleaciones de alta dilatación con un CET entre 16,0 y 17,3 x 10-6 K-1. Mediante un proceso de producción modificado se ha conseguido crear una novedosa cerámica cuya microestructura presenta, tras la cocción, una distribución especialmente homogénea de las fases cristalinas y vítreas en comparación con las cerámicas convencionales: la denominada estructura fina. Esta microestructura es la responsable de las extraordinarias propiedades de los materiales VITA VM 15. Al igual que en todos los materiales de VITA VM, el concepto de estratificación de VITA VM 15 se basa en el modelo natural, por lo que se diferencia claramente de todos los conceptos de estratificación conocidos. El material cromóforo BASE DENTINE es el componente principal y, por lo tanto, determinante para la reproducción del color dental.',
    imageUrl: 'assets/images/vita/45.jpg',
    idModal: 45,
    modalImageUrl: "assets/images/modalImages/vita/45.jpg"
  },
  {
    title: "VITA NP BOND",
    shortDescription: 'Bonder para mayor seguridad y para una estética mejorada en trabajos de recubrimiento de aleaciones dentales sin metales nobles. Gracias a la unión fiable entre la estructura de metal y la cerámica de recubrimiento (p. ej. VITA VMK MASTER y VITA VM 13), VITA NP BOND ayuda a evitar errores derivados de las diferencias de CET entre aleación y cerámica. VITA NP BOND (NP: non precious, ""no noble"" en inglés) mejora la unión entre la estructura de metal y la cerámica de recubrimiento especialmente en el caso de las aleaciones sin metales nobles y, de este modo, proporciona a los protésicos mayor seguridad en la confección de recubrimientos estéticos y estables a largo plazo.',
    imageUrl: 'assets/images/vita/46.jpg',
    idModal: 46,
    modalImageUrl: "assets/images/modalImages/vita/46.jpg"
  },
  {
    title: "VITAVM®11",
    shortDescription: 'VITA VM 11 es una cerámica de feldespato de estructura fina de bajo punto de fusión, especialmente desarrollada para la personalización de restauraciones de cerámica vítrea de silicato de litio reforzada con dióxido de circonio. La coordinación óptima entre ambos rangos CET garantiza una unión sin tensiones y por ende segura. En combinación con VITA SUPRINITY PC, y gracias a la excelente humectabilidad de la superficie de la nueva cerámica vítrea, se puede iniciar directamente el recubrimiento, prescindiendo de la cocción de liner o wash. El alto grado de translucidez y la coloración cálida de VITA VM 11, en combinación con el efecto opalescente de VITA SUPRINITY PC, se traducen en resultados de excelente estética con un juego cromático natural.',
    imageUrl: 'assets/images/vita/47.jpg',
    idModal: 47,
    modalImageUrl: "assets/images/modalImages/vita/47.jpg"
  },
  {
    title: "VITAVM®9",
    shortDescription: 'VITA VM 9 es una cerámica de feldespato de estructura fina altamente estética perfectamente adaptada al CET de las estructuras de dióxido de circonio (aprox. 10,5, como p. ej. VITA YZ), con total independencia de si es blanca, coloreada o translúcida. Además, puede utilizar el material VITA VM 9 para la personalización de restauraciones confeccionadas a partir de VITABLOCS MARK II y de pastillas de cerámica prensada VITA PM 9. Sus componentes principales son feldespatos potásicos y sódicos puros, los cuales aportan un efecto cromático brillante y unas propiedades físicas óptimas, entre ellas unos valores máximos de resistencia a la flexión. La superficie homogénea y lisa de VITA VM 9 facilita extraordinariamente las tareas de desbastado y pulido, sobre todo con la prótesis colocada in situ. De este modo se consiguen superficies lisas y bien selladas. Se reduce sensiblemente la adhesión de placa a la superficie de la cerámica, lo cual facilita al paciente el cuidado de la restauración de alta calidad. VITA VM 9 es un componente de gran calidad del innovador sistema VITA VM: la solución universal para todas las indicaciones habituales. Ya se trate de metalocerámica, cerámica sin metal o composite: las posibilidades son ilimitadas.Para el acabado personalizado dispone de los maquillajes VITA AKZENT Plus y VITA INTERNO.',
    imageUrl: 'assets/images/vita/48.jpg',
    idModal: 48,
    modalImageUrl: "assets/images/modalImages/vita/48.jpg"
  },
  {
    title: "VITAVM®LC",
    shortDescription: 'Uso sencillo. Resultados económicos y estéticos. Múltiples posibilidades: la familia de productos VITA VM LC ofrece todo lo que usted espera de un composite de recubrimiento para la elaboración extraoral de restauraciones. Independientemente de que utilice el composite en pasta o los materiales flow de baja viscosidad, o de que los combine a su elección: con VITA VM LC siempre conseguirá unos resultados brillantes. Muy próximos a la cerámica y, sin duda alguna, siempre de forma sistemática.',
    imageUrl: 'assets/images/vita/49.jpg',
    idModal: 49,
    modalImageUrl: "assets/images/modalImages/vita/49.jpg"
  },
  {
    title: "VITAVM®CC",
    shortDescription: 'VITA VM CC es un material polimerizable en frío sin material de relleno añadido. Es ideal para la confección extraoral de prótesis provisionales. VITA VM CC se caracteriza por unas excelentes propiedades de manipulación, como un comportamiento de mezcla y su excepcional fluidez facilitan el procesamiento. Gracias a la facilidad de pulido y a la homogénea estructura del material, se limitan las acumulaciones de placa en un alto grado. La superficie lisa de la prótesis provisional le proporciona una sensación agradable al paciente y le ofrece mayor comodidad.',
    imageUrl: 'assets/images/vita/50.jpg',
    idModal: 50,
    modalImageUrl: "assets/images/modalImages/vita/50.jpg"
  },
  {
    title: "VITA AKZENT® Plus",
    shortDescription: 'Los nuevos campos de aplicación requieren materiales nuevos y versátiles. Con los maquillajes VITA AKZENT Plus el usuario puede optimizar cromáticamente todos los tipos de cerámicas dentales de forma sencilla y eficiente, independientemente del CET de la restauración. Tanto si desea colorear la restauración internamente como si quiere caracterizarla superficialmente y aplicar una capa finísima, con los nuevos maquillajes VITA AKZENT Plus fluorescentes podrá hacerlo fácilmente y sin grandes esfuerzos. Los maquillajes VITA AKZENT Plus están disponibles en forma de polvo y como pastas listas para su uso; se adaptan así a distintos campos de aplicación, preferencias de elaboración y hábitos. Los materiales BODY STAINS y GLAZE, de aplicación en capa finísima, también están disponibles en spray, que garantiza un tratamiento extenso y uniforme de la superficie mediante la aplicación homogénea y sin que se desperdicie material. El nuevo pulverizador especial es ideal para una aplicación precisa.',
    imageUrl: 'assets/images/vita/51.jpg',
    idModal: 51,
    modalImageUrl: "assets/images/modalImages/vita/51.jpg"
  },
  {
    title: "VITA INTERNO®",
    shortDescription: 'Los polvos INTERNO de VITA proporcionan al ceramista los mejores requisitos para conseguir una reproducción todavía más perfecta de los efectos del color desde la profundidad. Gracias a un alto grado de fluorescencia (de forma análoga a como ocurre en la naturaleza) se consigue una mayor potencia lumínica de los colores y se intensifica el transporte de luz. Los polvos INTERNO de VITA pueden mezclarse con polvos de dentina y translúcidos. Con ello, se alcanza un efecto cromático mayor. Con los polvos INTERNO, pueden reproducirse especialmente bien los efectos del color naturales de la zona incisal (contrastes). Gracias a un alto grado de fluorescencia - de forma análoga a como ocurre en la naturaleza - se consigue una mayor potencia lumínica de los colores y se intensifica el transporte de luz. Los colores VITA INTERNO pueden utilizarse universalmente con todas las cerámicas de recubrimiento VITA.',
    imageUrl: 'assets/images/vita/52.jpg',
    idModal: 52,
    modalImageUrl: "assets/images/modalImages/vita/52.jpg"
  },
  {
    title: "VITA ENAMIC® STAINS",
    shortDescription: 'El VITA ENAMIC STAINS KIT incluye seis maquillajes más accesorios para la reproducción de matices cromáticos naturales de restauraciones confeccionadas a partir de cerámica híbrida. La unión de los maquillajes con la restauración se realiza mediante un proceso de polimerización. Para el sellado de la superficie está disponible el glaseado químico VITA ENAMIC GLAZE. De este modo se incrementa la durabilidad y el brillo de los maquillajes en el medio bucal. La elaboración se basa en 5 pasos muy sencillos: acondicionar la superficie de la restauración, mezclar los maquillajes y aplicarlos, realizar una polimerización intermedia, aplicar el glaseado químico y realizar la polimerización final.',
    imageUrl: 'assets/images/vita/53.jpg',
    idModal: 53,
    modalImageUrl: "assets/images/modalImages/vita/53.jpg"
  },
  {
    title: "VITA VACUMAT® 6000 M",
    shortDescription: 'El VITA VACUMAT 6000 M es un aparato de cocción totalmente automático y controlado por microprocesador, idóneo para todas las cocciones de cerámica dental. El horno, que destaca por su excelente calidad y estética, ofrece niveles máximos de calidad de cocción, seguridad de uso y confort. Gracias a su diseño ergonómico optimizado, requiere un espacio mínimo. Dos placas de enfriamiento integradas permiten depositar de manera segura las piezas de cocción. El atractivo diseño, disponible en acero inoxidable o en seis modernos y refinados colores de esmaltado, atraerá todas las miradas en su lugar de trabajo.',
    imageUrl: 'assets/images/vita/54.jpg',
    idModal: 54,
    modalImageUrl: "assets/images/modalImages/vita/54.jpg"
  },
  {
    title: "VITA V60 i-Line®",
    shortDescription: 'VITA es sinónimo de excelente calidad y tecnología acreditada. Con el VITA V60 i-Line, VITA hace el debido honor a estos valores. El VITA V60 i-Line es el resultado de priorizar dos aspectos fundamentales: unos resultados de cocción extraordinarios y duraderos y la facilidad de uso.',
    imageUrl: 'assets/images/vita/55.jpg',
    idModal: 55,
    modalImageUrl: "assets/images/modalImages/vita/55.jpg"
  },
  {
    title: "VITA VACUMAT® 6000 MP",
    shortDescription: 'El VITA VACUMAT 6000 MP está construido para garantizarle unos resultados de prensado seguros y fiables. Este aparato de prensado combinado aúna todas las ventajas de un moderno horno de cocción y prensado, a la vez que su diseño compacto establece un nuevo estándar. El VITA VACUMAT 6000 MP cuenta con todos los programas y funciones del VITA VACUMAT 6000 M. Además, es apto para el prensado de todo tipo de cerámicas prensadas disponibles en el mercado y para los más diversos sistemas de muflas. La detección automática de la cantidad de pastillas utilizada impide un prensado defectuoso y desperfectos en la mufla de cocción debido a una carga insuficiente (VITA PressControl).',
    imageUrl: 'assets/images/vita/56.jpg',
    idModal: 56,
    modalImageUrl: "assets/images/modalImages/vita/56.jpg"
  },
  {
    title: "VITA ZYRCOMAT® 6100 MS",
    shortDescription: 'Íntegramente desarrollado y fabricado en Alemania, el VITA ZYRCOMAT 6100 MS garantiza la calidad y la fiabilidad acostumbradas de VITA. El horno de sinterización ofrece tres modos de sinterización libremente seleccionables: alta velocidad, convencional y definido por el usuario. Gracias a ello, se mantiene la flexibilidad en todo momento. Adaptado a los materiales VITA, así como a todas las cerámicas dentales para estructuras con base de ZrO2 y Al2O3 disponibles en el mercado, este horno de sinterización de alta temperatura establece un nuevo referente en cuanto a facilidad de manejo y ergonomía. Una varilla luminosa de LED y una serie de señales acústicas libremente ajustables informan de forma fiable, incluso desde la distancia, del estado de la cocción. También el sistema de escape de aire marca nuevas pautas: gracias al diseño inteligente del aparato, el calor ascendente no afecta a los componentes electrónicos. De este modo se evitan eficazmente los daños a los elementos calefactores y electrónicos.',
    imageUrl: 'assets/images/vita/57.jpg',
    idModal: 57,
    modalImageUrl: "assets/images/modalImages/vita/57.jpg"
  },
  {
    title: "VITA ENAMIC® Polishing Set",
    shortDescription: 'Los kits de Instrumento de pulidoes VITA ENAMIC se han desarrollado para tratar la superficie de restauraciones confeccionadas con cerámica híbrida de forma segura, económica y adecuada a las características del material, pudiendo utilizarse tanto en la clínica como en el laboratorio dental. Los kits incluyen diversos Instrumento de pulidoes para el pulido previo y el pulido de alto brillo. Con estos instrumentos pueden pulirse las superficies masticatorias, cúspides, fosas y puntos de contacto de la restauración de forma respetuosa con el material. En el resultado final estos instrumentos de pulido consiguen unas superficies con un excelente nivel de brillo.',
    imageUrl: 'assets/images/vita/58.jpg',
    idModal: 58,
    modalImageUrl: "assets/images/modalImages/vita/58.jpg"
  },
  {
    title: "VITA SUPRINITY® Polishing Set",
    shortDescription: 'Los kits de pulido VITA SUPRINITY han sido desarrollados para tratar la superficie de restauraciones confeccionadas con cerámica de silicato de litio reforzada con dióxido de circonio (ZLS) de forma segura, económica y adecuada a las características del material, pudiendo utilizarse tanto en la clínica como en el laboratorio dental.Los kits incluyen diversos Instrumento de pulidoes para el pulido previo y el pulido de alto brillo. Con estos instrumentos pueden pulirse las superficies oclusales, cúspides, fosas y puntos de contacto de la restauración de forma respetuosa con el material. En el resultado final estos instrumentos de pulido consiguen unas superficies con un excelente nivel de brillo.',
    imageUrl: 'assets/images/vita/59.jpg',
    idModal: 59,
    modalImageUrl: "assets/images/modalImages/vita/59.jpg"
  },  
];

module.exports = {
  vita
};