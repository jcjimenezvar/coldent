const bego = [
  {
    title: "Bellavest © SH",
    shortDescription: "",
    imageUrl: "assets/images/bego/1.jpg",
    idModal: 1,
    modalImageUrl: "assets/images/modalImages/bego/1.jpg"
  },
  {
    title: "",
    shortDescription: "",
    imageUrl: "assets/images/bego/2.jpg",
    idModal: 2,
    modalImageUrl: "assets/images/modalImages/bego/2.jpg"
  },
  {
    title: "WiroFine",
    shortDescription: "",
    imageUrl: "assets/images/bego/3.jpg",
    idModal: 3,
    modalImageUrl: "assets/images/modalImages/bego/3.jpg"
  },
  {
    title: "Wiropaint Plus",
    shortDescription: "",
    imageUrl: "assets/images/bego/4.jpg",
    idModal: 4,
    modalImageUrl: "assets/images/modalImages/bego/4.jpg"
  },
  {
    title: "Aurofilm",
    shortDescription: "",
    imageUrl: "assets/images/bego/5.jpg",
    idModal: 5,
    modalImageUrl: "assets/images/modalImages/bego/5.jpg"
  },
  {
    title: "",
    shortDescription: "",
    imageUrl: "assets/images/bego/6.jpg",
    idModal: 6,
    modalImageUrl: "assets/images/modalImages/bego/6.jpg"
  },
  {
    title: "cera oclusal 40017",
    shortDescription: "",
    imageUrl: "assets/images/bego/7.jpg",
    idModal: 7,
    modalImageUrl: "assets/images/modalImages/bego/7.jpg"
  },
  {
    title: "cera oclusal 40018",
    shortDescription: "",
    imageUrl: "assets/images/bego/8.jpg",
    idModal: 8,
    modalImageUrl: "assets/images/modalImages/bego/8.jpg"
  },
  {
    title: "",
    shortDescription: "",
    imageUrl: "assets/images/bego/9.jpg",
    idModal: 9,
    modalImageUrl: "assets/images/modalImages/bego/9.jpg"
  },
  {
    title: "Wironit LA",
    shortDescription: "",
    imageUrl: "assets/images/bego/10.jpg",
    idModal: 10,
    modalImageUrl: "assets/images/modalImages/bego/10.jpg"
  },
  {
    title: "Wiron 99",
    shortDescription: "",
    imageUrl: "assets/images/bego/11.jpg",
    idModal: 11,
    modalImageUrl: "assets/images/modalImages/bego/11.jpg"
  },
  {
    title: "Wiron light",
    shortDescription: "",
    imageUrl: "assets/images/bego/12.jpg",
    idModal: 12,
    modalImageUrl: "assets/images/modalImages/bego/12.jpg"
  },
  {
    title: "",
    shortDescription: "",
    imageUrl: "assets/images/bego/13.jpg",
    idModal: 13,
    modalImageUrl: "assets/images/modalImages/bego/13.jpg"
  },
  {
    title: "Cera para Modela",
    shortDescription: "",
    imageUrl: "assets/images/bego/14.jpg",
    idModal: 14,
    modalImageUrl: "assets/images/modalImages/bego/14.jpg"
  },
  {
    title: "Cera Cervical",
    shortDescription: "",
    imageUrl: "assets/images/bego/15.jpg",
    idModal: 15,
    modalImageUrl: "assets/images/modalImages/bego/15.jpg"
  },
  {
    title: "Holo de Cera para Bebederos",
    shortDescription: "",
    imageUrl: "assets/images/bego/16.jpg",
    idModal: 16,
    modalImageUrl: "assets/images/modalImages/bego/16.jpg"
  }
];

module.exports = {
  bego
};
