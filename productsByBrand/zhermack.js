const zhermack = [
    {
      title: "Arena zeta",
      shortDescription: 'Caracteristicas: Alto grado de cribado, Paquete de recarga simple de aluminio de tres capas y 1 kg, Diferentes tamaños de grano disponibles. La calidad de la materia prima es proporcionada por un alto grado de tamizado. El paquete de 1 kg protege las arenas de la humedad, la causa principal del mal funcionamiento del chorro de arena. Los diversos tamaños de grano son adecuados para varios tipos de arenado y acabado',
      imageUrl: 'assets/images/zhermack/1.jpg',
      idModal: 1,
      modalImageUrl: "assets/images/modalImages/zhermack/1.jpg"
    },
    {
        title: "Solución Zeta 7",
        shortDescription: 'Caracteristicas: Amplio espectro de acción desarrollado y probado de acuerdo con las últimas normas europeas sobre desinfección, Compatibilidad con materiales para la toma de impresión (siliconas de adición y condensación, alginato, poliéter, polisulfuro y polivinilo). Eficacia : amplia protección para profesionales en consultorios y laboratorios dentales. Alto rendimiento: respeta la estabilidad dimensional de las características de las impresiones y su compatibilidad con el yeso y mejora la precisión en la reproducción de modelos de yeso.',
        imageUrl: 'assets/images/zhermack/2.jpg',
        idModal: 2,
        modalImageUrl: "assets/images/modalImages/zhermack/2.jpg"
      },
      {
        title: "Zeta 7 Spray",
        shortDescription: 'Caracteristicas: Amplio espectro de acción desarrollado y probado de acuerdo con las últimas normas europeas sobre desinfección, Compatibilidad con materiales para la toma de impresión (siliconas de adición y condensación, alginato, poliéter, polisulfuro y polivinilo). Eficacia: amplia protección para profesionales en consultorios y laboratorios dentales. Alto rendimiento: respeta la estabilidad dimensional de las características de las impresiones y su compatibilidad con el yeso y mejora la precisión en la reproducción de modelos de yeso.',
        imageUrl: 'assets/images/zhermack/3.jpg',
        idModal: 3,
        modalImageUrl: "assets/images/modalImages/zhermack/3.jpg"
      },
      {
        title: "Algitray",
        shortDescription: 'Algitray es un limpiador de pH neutro específico para la eliminación de residuos de alginato de las bandejas de impresión y el instrumento. Eficacia: ayuda a eliminar trazas de alginato y yeso incluso en áreas menos accesibles, Protección de materiales: fórmula no agresiva.',
        imageUrl: 'assets/images/zhermack/4.jpg',
        idModal: 4,
        modalImageUrl: "assets/images/modalImages/zhermack/4.jpg"
      },
      {
        title: "Gypstray",
        shortDescription: 'Gypstray es una solución lista para usar para la eliminación de residuos de yeso de las bandejas de impresión, espátulas u otros instrumentos. Eficacia : ayuda a eliminar trazas de alginato y yeso incluso en áreas menos accesibles. Protección de materiales: fórmula no agresiva',
        imageUrl: 'assets/images/zhermack/5.jpg',
        idModal: 5,
        modalImageUrl: "assets/images/modalImages/zhermack/5.jpg"
      },
      {
        title: "Platinum 75 CAD",
        shortDescription: 'Características: Reproducción exacta de los detalles, Dureza 75 Shore A, Proporción de mezcla: 1:1. Tiempo de trabajo más rápido para mejorar la eficiencia en el laboratorio. Fresable lo que permite un fácil acabado. Escaneable sin usar sprays reflectantes',
        imageUrl: 'assets/images/zhermack/6.jpg',
        idModal: 6,
        modalImageUrl: "assets/images/modalImages/zhermack/6.jpg"
      },
      {
        title: "Occlufast CAD",
        shortDescription: 'Características: Reproducción exacta de los detalles, Gran dureza: 95 Shore A, Proporción de mezcla: 1:1, Tiempo de polimerización: 60 segundos. Tiempo de polimerización reducido. Escaneable sin usar sprays reflectantes. Fresable lo que permite un fácil acabado. Tiempo de polimerización: podría ser más prolongada según las condiciones ambientales.',
        imageUrl: 'assets/images/zhermack/7.jpg',
        idModal: 7,
        modalImageUrl: "assets/images/modalImages/zhermack/7.jpg"
      },
      {
        title: "Gingifast CAD",
        shortDescription: 'Características: Dos versiones de dureza: Elastic 40 Shore A, Rigid 70 Shore A, Tiempos de trabajo rápidos, Proporción de mezcla: 1:1. Ahorro de tiempo en la obtención de datos con CAD/CAM. Mayor precisión en comparación con aquella obtenida al utilizar sprays reflectantes. Compatible con técnicas tanto directa como indirecta. Menos desperdicio de material gracias a las puntas de mezcla más reducidas',
        imageUrl: 'assets/images/zhermack/8.jpg',
        idModal: 8,
        modalImageUrl: "assets/images/modalImages/zhermack/8.jpg"
      },
      {
        title: "Elite Master",
        shortDescription: 'Características: Escaneable sin usar sprays reflectantes, Libre de formaldehídos, Resistente al astillado. Ahorro de tiempo en la obtención de datos con escáneres ópticos. Mayor precisión en comparación con la obtenida al utilizar sprays reflectantes. Entorno más limpio.',
        imageUrl: 'assets/images/zhermack/9.jpg',
        idModal: 9,
        modalImageUrl: "assets/images/modalImages/zhermack/9.jpg"
      },
      {
        title: "Elite Rock",
        shortDescription: 'Características: También disponible en versión rápida para optimizar el tiempo de trabajo, Baja expansión, incluso después de 48 horas, Reproducción exacta de los detalles. Ahorro de tiempo en la obtención de datos con escáneres ópticos. Mayor precisión en comparación con la obtenida al utilizar sprays reflectantes. Entorno más limpio.',
        imageUrl: 'assets/images/zhermack/10.jpg',
        idModal: 10,
        modalImageUrl: "assets/images/modalImages/zhermack/10.jpg"
      },
      {
        title: "Elite Base",
        shortDescription: 'Características: Baja expansión, incluso después de 48 horas, Reproducción exacta de los detalles. Tixotrópico para simplificar la realización del modelo. Gran fluidez, incluso sin vibración',
        imageUrl: 'assets/images/zhermack/11.jpg',
        idModal: 11,
        modalImageUrl: "assets/images/modalImages/zhermack/11.jpg"
      },
      {
        title: "Elite Stone",
        shortDescription: 'Características: Baja expansión, incluso después de 48 horas, Reproducción exacta de los detalles. Tixotrópico para simplificar la realización del modelo. Gran fluidez, incluso sin vibración',
        imageUrl: 'assets/images/zhermack/12.jpg',
        idModal: 12,
        modalImageUrl: "assets/images/modalImages/zhermack/12.jpg"
      },
      {
        title: "Elite Arti – Elite Arti Fast",
        shortDescription: 'Características: Tixotrópico, Expansión mínima, También disponible en versión rápida, Color blanco. Expansión mínima para mantener la oclusión inalterada. Tixotrópico para simplificar la colocación en el articulador',
        imageUrl: 'assets/images/zhermack/13.jpg',
        idModal: 13,
        modalImageUrl: "assets/images/modalImages/zhermack/13.jpg"
      },
      {
        title: "Elite Model – Elite Model Fast",
        shortDescription: 'Características: Baja expansión, Elevadas características mecánicas, Uso universal. Sencilla realización del modelo gracias a su alta tixotropía. Extrema versatilidad debido a sus características superiores en comparación con otros yesos del mismo tipo.',
        imageUrl: 'assets/images/zhermack/14.jpg',
        idModal: 14,
        modalImageUrl: "assets/images/modalImages/zhermack/14.jpg"
      },
      {
        title: "Elite Ortho",
        shortDescription: 'Características: Blanco brillante, Tixotrópico, Baja expansión. Específico para modelos de ortodoncia. Idóneo para a modelos del estudio.',
        imageUrl: 'assets/images/zhermack/15.jpg',
        idModal: 15,
        modalImageUrl: "assets/images/modalImages/zhermack/15.jpg"
      },
      {
        title: "Elite Double 8",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/16.jpg',
        idModal: 16,
        modalImageUrl: "assets/images/modalImages/zhermack/16.jpg"
      },
      {
        title: "Elite Double 22",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/17.jpg',
        idModal: 17,
        modalImageUrl: "assets/images/modalImages/zhermack/17.jpg"
      },
      {
        title: "Elite Double 22 Extra Fast",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/18.jpg',
        idModal: 18,
        modalImageUrl: "assets/images/modalImages/zhermack/18.jpg"
      },
      {
        title: "Elite Double 16 Fast",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/19.jpg',
        idModal: 19,
        modalImageUrl: "assets/images/modalImages/zhermack/19.jpg"
      },
      {
        title: "Elite Double 22 Fast",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/20.jpg',
        idModal: 20,
        modalImageUrl: "assets/images/modalImages/zhermack/20.jpg"
      },
      {
        title: "Elite Double 32",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/21.jpg',
        idModal: 21,
        modalImageUrl: "assets/images/modalImages/zhermack/21.jpg"
      },
      {
        title: "Elite Double 32 Fast",
        shortDescription: 'Características: Resistencia al estiramiento y el desgarro, incluso con espesores finos, Reproducción exacta de los detalles, Gran fluidez, Alta estabilidad dimensional con el paso del tiempo, Elevada recuperación elástica. Optimización de los tiempos de trabajo, particularmente en comparación con el duplicado con hidrocoloides (tiempo de polimerización de 5:00, 10:00, 20:00). El modelo se puede duplicar varias veces gracias a la estabilidad dimensional a lo largo del tiempo y a la recuperación elástica. Compatible con yesos, resinas poliuretánicas, resinas acrílicas y revestimientos fosfáticos y a base de alcohol. Gran fluidez: no es necesario mezclar al vacío.',
        imageUrl: 'assets/images/zhermack/22.jpg',
        idModal: 22,
        modalImageUrl: "assets/images/modalImages/zhermack/22.jpg"
      },
      {
        title: "Villacryl IT",
        shortDescription: 'Características: Rígida y estable, No contiene metales pesados, 2 colores: verde y rosa. Fácil preparación y acabado. Listo para usar inmediatamente después de mezclar. No se pega a las manos.',
        imageUrl: 'assets/images/zhermack/23.jpg',
        idModal: 23,
        modalImageUrl: "assets/images/modalImages/zhermack/23.jpg"
      },
      {
        title: "Elite LC Tray ROUND",
        shortDescription: 'Características: Polimerizadas tanto con la luz halógena como con UV (3 a 5 minutos), Estables con la luz natural o artificial durante aproximadamente 20 minutos, Disponibles en tres colores: azul, blanco y rosa, Contracción mínima luego de la polimerización. Fáciles de modelar, no se pegan. Rápidas, ahorran tiempo con respecto a las resinas autopolimerizables tradicionales. Espesor uniforme',
        imageUrl: 'assets/images/zhermack/24.jpg',
        idModal: 24,
        modalImageUrl: "assets/images/modalImages/zhermack/24.jpg"
      },
      {
        title: "Elite LC Tray",
        shortDescription: 'Características: Polimerizadas tanto con la luz halógena como con UV (3 a 5 minutos), Estables con la luz natural o artificial durante aproximadamente 20 minutos, Disponibles en tres colores: azul, blanco y rosa, Contracción mínima después de la polimerización. Fáciles de modelar, no son pegajosas. Rápidas, ahorran tiempo con respecto a las resinas autopolimerizables tradicionales. Espesor uniforme.',
        imageUrl: 'assets/images/zhermack/25.jpg',
        idModal: 25,
        modalImageUrl: "assets/images/modalImages/zhermack/25.jpg"
      },
      {
        title: "Zetalabor",
        shortDescription: 'Características: Resistente a temperaturas de hasta 100 °C, Buena precisión, Siliconas para mezclar únicamente con catalizador en gel. Ahorro de tiempo. Excelente relación calidad/precio. Para uso con técnicas con resinas en caliente y en frío.',
        imageUrl: 'assets/images/zhermack/26.jpg',
        idModal: 26,
        modalImageUrl: "assets/images/modalImages/zhermack/26.jpg"
      },
      {
        title: "Titanium",
        shortDescription: 'Características: Resistente a temperaturas de hasta 100 °C, Buena precisión, Siliconas para mezclar únicamente con catalizador en gel. Ahorro de tiempo. Excelente relación calidad/precio. Para uso con técnicas con resinas en caliente y en frío.',
        imageUrl: 'assets/images/zhermack/27.jpg',
        idModal: 27,
        modalImageUrl: "assets/images/modalImages/zhermack/27.jpg"
      },
      {
        title: "Elite Transparent",
        shortDescription: 'Características: Altamente transparente, Baja viscosidad, Gran dureza final 72 Shore A. Tiempos de trabajo reducidos. Debido a la fotopolimerización en una atmósfera libre de oxígeno, la superficie de los composites y de las resinas es más compacta, libre de capa de dispersión. Fácil y rápido de aplicar. Posibilidad de conservar llaves para uso futuro.',
        imageUrl: 'assets/images/zhermack/28.jpg',
        idModal: 28,
        modalImageUrl: "assets/images/modalImages/zhermack/28.jpg"
      },
      {
        title: "Platinum 85 TOUCH",
        shortDescription: 'Características: Disponible en una amplia gama de durezas, Reproducción exacta de los detalles, Resistente a temperaturas de hasta 200 °C, Proporción de mezcla: 1:1. Se puede utilizar para procesos que se prolongan en el tiempo (por ejemplo, implantes con encerado diagnóstico) sin alterar las referencias dimensionales. Se puede utilizar con materiales termoplásticos gracias a su resistencia al calor de hasta 200 °C. Manipulación sencilla e incluso fresable.',
        imageUrl: 'assets/images/zhermack/29.jpg',
        idModal: 29,
        modalImageUrl: "assets/images/modalImages/zhermack/29.jpg"
      },
      {
        title: "Platinum 95",
        shortDescription: 'Características: Disponible en una amplia gama de durezas, Reproducción exacta de los detalles, Resistente a temperaturas de hasta 200 °C, Proporción de mezcla: 1:1. Se puede utilizar para procesos que se prolongan en el tiempo (por ejemplo, implantes con encerado diagnóstico) sin alterar las referencias dimensionales. Se puede utilizar con materiales termoplásticos gracias a su resistencia al calor de hasta 200 °C. Manipulación sencilla e incluso fresable',
        imageUrl: 'assets/images/zhermack/30.jpg',
        idModal: 30,
        modalImageUrl: "assets/images/modalImages/zhermack/30.jpg"
      },
      {
        title: "Platinum 75 CAD",
        shortDescription: 'Características: Disponible en una amplia gama de durezas, Reproducción exacta de los detalles, Resistente a temperaturas de hasta 200 °C, Proporción de mezcla: 1:1. Se puede utilizar para procesos que se prolongan en el tiempo (por ejemplo, implantes con encerado diagnóstico) sin alterar las referencias dimensionales. Se puede utilizar con materiales termoplásticos gracias a su resistencia al calor de hasta 200 °C. Manipulación sencilla e incluso fresable',
        imageUrl: 'assets/images/zhermack/31.jpg',
        idModal: 31,
        modalImageUrl: "assets/images/modalImages/zhermack/31.jpg"
      },
      {
        title: "Platinum 85",
        shortDescription: 'Características: Disponible en una amplia gama de durezas, Reproducción exacta de los detalles, Resistente a temperaturas de hasta 200 °C, Proporción de mezcla: 1:1. Se puede utilizar para procesos que se prolongan en el tiempo (por ejemplo, implantes con encerado diagnóstico) sin alterar las referencias dimensionales. Se puede utilizar con materiales termoplásticos gracias a su resistencia al calor de hasta 200 °C. Manipulación sencilla e incluso fresable',
        imageUrl: 'assets/images/zhermack/32.jpg',
        idModal: 32,
        modalImageUrl: "assets/images/modalImages/zhermack/32.jpg"
      },
      {
        title: "Gingifast Elastic",
        shortDescription: 'Características: Diversas durezas: Elastic 40 Shore A, Rigid 70 Shore A, Tiempo de polimerización: 10:00, Proporción de mezcla: 1:1. Compatible con las diversas técnicas utilizadas para realizar falsas encías (directa e indirecta). Resultados estéticos excelentes. Con la fórmula Gingifast CAD, se pueden utilizar puntas mezcladoras muy pequeñas, lo cual reduce el desperdicio de silicona.',
        imageUrl: 'assets/images/zhermack/33.jpg',
        idModal: 33,
        modalImageUrl: "assets/images/modalImages/zhermack/33.jpg"
      },
      {
        title: "Gingifast CAD Elastic and Rigid",
        shortDescription: 'Características: Diversas durezas: Elastic 40 Shore A, Rigid 70 Shore A, Tiempo de polimerización: 10:00, Proporción de mezcla: 1:1. Compatible con las diversas técnicas utilizadas para realizar falsas encías (directa e indirecta). Resultados estéticos excelentes. Con la fórmula Gingifast CAD, se pueden utilizar puntas mezcladoras muy pequeñas, lo cual reduce el desperdicio de silicona.',
        imageUrl: 'assets/images/zhermack/34.jpg',
        idModal: 34,
        modalImageUrl: "assets/images/modalImages/zhermack/34.jpg"
      },
      {
        title: "Gingifast Rigid",
        shortDescription: 'Características: Diversas durezas: Elastic 40 Shore A, Rigid 70 Shore A, Tiempo de polimerización: 10:00, Proporción de mezcla: 1:1. Compatible con las diversas técnicas utilizadas para realizar falsas encías (directa e indirecta). Resultados estéticos excelentes. Con la fórmula Gingifast CAD, se pueden utilizar puntas mezcladoras muy pequeñas, lo cual reduce el desperdicio de silicona.',
        imageUrl: 'assets/images/zhermack/35.jpg',
        idModal: 35,
        modalImageUrl: "assets/images/modalImages/zhermack/35.jpg"
      },
      {
        title: "Villacryl SP",
        shortDescription: 'Características: No contiene metales pesados, Biológicamente inerte, 3 colores: rosa veteado (V4), blanco rosáceo veteado (V2), transparente (0). Fabricación de prótesis más rápida al realizar el vaciado con una matriz de silicona. Fácil preparación y acabado. Buena capacidad para ocultar las partes metálicas.',
        imageUrl: 'assets/images/zhermack/36.jpg',
        idModal: 36,
        modalImageUrl: "assets/images/modalImages/zhermack/36.jpg"
      },
      {
        title: "Villacryl H Plus – Villacryl H Rapid FN",
        shortDescription: 'Características: No contiene metales pesados, Biológicamente inerte, Alta resistencia mecánica, Villacryl H Plus se ofrece en 5 colores: rosa veteado (V4), rosa (T4), blanco rosáceo veteado (V2), transparente (0), rosa oscuro veteado (V3), Villacryl H Rapid FN: rosa veteado (V4). Villacryl H Rapid FN: procesamiento más rápido (permite ahorrar unos 60 minutos en la fabricación de la prótesis). Villacryl H Plus y Villacryl H Rapid FN pueden ser rebasadas con materiales blandos o duros. Estabilidad cromática en el tiempo. Prótesis altamente estéticas.',
        imageUrl: 'assets/images/zhermack/37.jpg',
        idModal: 37,
        modalImageUrl: "assets/images/modalImages/zhermack/37.jpg"
      },
      {
        title: "Villacryl S",
        shortDescription: 'Características: No contiene metales pesados, Biológicamente inerte, 4 colores: rosa veteado (V4), rosa (T4), blanco rosáceo veteado (V2), transparente (0). Fácil preparación y acabado. Reparaciones rápida gracias a la excelente capacidad de adhesión a la resina acrílica termopolimerizable. Reparaciones estéticas gracias a la guía de colores de Villacryl H Plus, Villacryl H Rapid y Villacryl SP.',
        imageUrl: 'assets/images/zhermack/38.jpg',
        idModal: 38,
        modalImageUrl: "assets/images/modalImages/zhermack/38.jpg"
      },
      {
        title: "Elite Vest",
        shortDescription: 'Características: Precalentamiento rápido o tradicional, Fórmulas de polvo fino, sin carbono con expansión adaptable a la técnica de trabajo, Excelente precisión, especialmente en implantes y puentes largos. Fácil de usar: simplemente dos productos para todos los tipos de trabajo. Flexibilidad: la expansión puede adaptarse a las diferentes necesidades. Tiempo de trabajo reducido: superficie de aleación lisa trás el colado.',
        imageUrl: 'assets/images/zhermack/39.jpg',
        idModal: 39,
        modalImageUrl: "assets/images/modalImages/zhermack/39.jpg"
      },
      {
        title: "Elite Mix",
        shortDescription: 'Características: Diez programas diferentes que pueden programarse con los siguientes parámetros: Tiempo de espatulado, Velocidad de espatulado de entre 100 y 450 rpm, Pre-espatulado, Pre-vacio previo al espatulado, Intervalo de inversión de la dirección de rotación de la espátula, Grado de vacío, Control electrónico del vacío y doble filtro. Vacío generado por la bomba de membrana dentro del mezclador, Se puede montar en la pared o la mesa con soporte óptimo. La bomba de vacío “libre de aceite” no requiere mantenimiento. El contenedor para mezclar Elite Mix tiene superficies internas redondeadas para facilitar la limpieza y el recogido de la mezcla. Diez programas personalizables, incluidos seis programas predeterminados para yesos Zhermack',
        imageUrl: 'assets/images/zhermack/40.jpg',
        idModal: 40,
        modalImageUrl: "assets/images/modalImages/zhermack/40.jpg"
      },
      {
        title: "Doublemix",
        shortDescription: 'Características: Acoplamiento rápido de los envases con válvula de cierre de flujo, Placa ajustable para sostener la punta mezcladora, Contenedores transparentes opcionales de 2 kg para poder utilizar con las principales siliconas del mercado. Duplicado de modelos en solo cinco minutos (usando Elite Double Extra Fast). Proporción de mezcla siempre correcta, gracias a la bomba de dosificación con una proporción 1:1. Permite que no haya desperdicio de silicona. El sistema de conexión rápida y las válvulas de cierre de flujo evitan la formación de burbujas de aire durante la aplicación de la silicona.',
        imageUrl: 'assets/images/zhermack/41.jpg',
        idModal: 41,
        modalImageUrl: "assets/images/modalImages/zhermack/41.jpg"
      },
      {
        title: "Vap 8 – VAP 8A",
        shortDescription: 'Características: Boquilla fija de 8 bares para vapor seco, Dispositivo de seguridad antiestática, Presión de trabajo máxima de 8 bares, Sistema electrónico de protección de la resistencia: cuando no queda agua, el sistema interrumpe la alimentación de energía para proteger la resistencia, TSS (Sistema triple de seguridad), Depósito de 4 litros de acero inoxidable, VAP 8: solo vapor, VAP 8A: agua y vapor. Presión elevada para responder a las exigencias de limpieza del laboratorio o de la clínica dental. Fiabilidad debida a la elección de componentes de alta calidad y a la experiencia en la fabricación de generadores de vapor. Seguridad certificada CE (EMC, LVD y PED), EN61010.',
        imageUrl: 'assets/images/zhermack/42.jpg',
        idModal: 42,
        modalImageUrl: "assets/images/modalImages/zhermack/42.jpg"
      },
      {
        title: "VAP 6 – VAP 6 A",
        shortDescription: 'Características: Presión de trabajo máxima de 6 bar, Sistema electrónico de protección de la resistencia: cuando se acaba el agua, el sistema interrumpe la alimentación de energía a la resistencia, TSS (Sistema triple de seguridad), Depósito de 4 litros de acero inoxidable, VAP 6: solo vapor, VAP 6A: agua y vapor. Componentes de alta calidad que aportan fiabilidad en el tiempo. Fácil de usar. Seguridad certificada CE (EMC, LVD y PED), EN61010.',
        imageUrl: 'assets/images/zhermack/43.jpg',
        idModal: 43,
        modalImageUrl: "assets/images/modalImages/zhermack/43.jpg"
      },
      {
        title: "Zeta Sonic",
        shortDescription: 'Características: Cuba de acero inoxidable, Pantalla a prueba de agua, Innovadora función de barrido y desgaseado, Pantalla para regular el tiempo y la temperatura, Apagado automático (luego de 12 horas). Práctico: ayuda a reducir el tiempo de limpieza de las prótesis con residuos de yeso u otros materiales. Acelera la acción de los productos que eliminan el alginato y el yeso. Máquina inteligente: mantiene la temperatura establecida durante su uso y se apaga automáticamente trás 12 horas.',
        imageUrl: 'assets/images/zhermack/44.jpg',
        idModal: 44,
        modalImageUrl: "assets/images/modalImages/zhermack/44.jpg"
      },
      {
        title: "SAB 1000",
        shortDescription: 'Características: Alta potencia y fiable, Filtrado garantizado de hasta 0,3 μm, Filtro de papel de 2 capas, idóneo para arenas extrafinas. Excelente aspiración de la arena que se encuentra en la cámara de arenado. Fiable, de fácil limpieza y mantenimiento. Potencia de aspiración elevada.',
        imageUrl: 'assets/images/zhermack/45.jpg',
        idModal: 45,
        modalImageUrl: "assets/images/modalImages/zhermack/45.jpg"
      },
      {
        title: "AD 8",
        shortDescription: 'Características: Realizado en acero inoxidable, Suministro continuo de agua garantizado, incluso durante la regeneración de las bolas de resina, Regeneración mediante NaCl (sal gruesa de cocina). El desmineralizador prolonga la vida útil del generador de vapor. Produce agua desminerilazada para otros usos en el laboratorio (por ej. para mezclar yeso) y, por lo tanto, mejora la calidad de la mezcla. Bajo coste de mantenimiento (regeneración de resinas usando sal gruesa de cocina).',
        imageUrl: 'assets/images/zhermack/46.jpg',
        idModal: 46,
        modalImageUrl: "assets/images/modalImages/zhermack/46.jpg"
      },
      {
        title: "California",
        shortDescription: 'Características: Sistema de pinzamiento del tubo para detener inmediatamente el flujo de aire y arena, lo cual reduce sensiblemente el consumo de arena, Flujo constante. Diseñada hasta el más mínimo detalle para obtener un flujo constante incluso con baja presión: Doble flujo de aire en el cilindro (únicamente cilindro de 25 a 70 μm), Doble flujo de aire en la base del cilindro (todos los cilindros), Tubos calibrados y boquilla parabólica, Diseñada para durar. Diseño técnico pensado para mejorar su fiabilidad: tubos con entrada directa a la cámara, sistema neumático de gran calidad. Bajo consumo. Flujo constante incluso con baja presión (particularmente indicada para cerámica prensada y elementos estéticos, como las carillas). Zhermack recomienda el uso de: Zeta Sand: óxido de aluminio y perlas de vidrio para arenado. SAB 1000: aspirador para arenadoras.',
        imageUrl: 'assets/images/zhermack/47.jpg',
        idModal: 47,
        modalImageUrl: "assets/images/modalImages/zhermack/47.jpg"
      },
      {
        title: "SAND S 24 R",
        shortDescription: 'Características: Sistema de reciclado, equipado con boquilla de carburo reistente Widia (diám. int. 3 mm), el cual permite reutilizar el material depositado en la parte interior en operaciones de desbastado, Equipada con dos cilindros con una capacidad aproximada de 0,5 litros cada uno, Selector de cilindros en el interior de la cámara. Bajo consumo de arena gracias al sistema de reciclado. Gran fiabilidad y bajo mantenimiento por una baja inversión.',
        imageUrl: 'assets/images/zhermack/48.jpg',
        idModal: 48,
        modalImageUrl: "assets/images/modalImages/zhermack/48.jpg"
      },
      {
        title: "DM – DE",
        shortDescription: 'Características: DM: Sistema de calentamiento por tubos: los elementos de calentamiento se alojan en tubos de alúmina especiales para ofrecer una alto rendimiento y gran fiabilidad, Diez programas con cuatro fases programables. Posibilidad de programar la temperatura de apagado del aspirador, Encendido retardado, con la posibilidad de programar el día y la hora de encendido del horno, Extractor de humos opcional. DE: Programa único que consta de dos fases de mantenimiento, cada una de las cuales ofrece la posibilidad de programar lo siguiente: Temperatura, Tiempo de mantenimiento, Velocidad de subida dela temperatura, Encendido retardado. El sistema de calentamiento por tubos hace que el intercambio de calor sea más eficiente que en una mufla tradicional. Menor consumo de electricidad. Fiabilidad en el tiempo. Posibilidad de modificar un programa, incluso mientras se está ejecutando.',
        imageUrl: 'assets/images/zhermack/49.jpg',
        idModal: 49,
        modalImageUrl: "assets/images/modalImages/zhermack/49.jpg"
      },
      {
        title: "Cyclope",
        shortDescription: 'Características: Al ser un aparato eléctrico no requiere conexión al suministro de gas, El instrumento se calienta en tan solo dos segundos, Dos capuchones de silicona protegen la cámara contra arañazos y residuos de cera. Seguridad: ausencia de riesgo de quemaduras y mejor calidad del aire en el laboratorio dental gracias a la eliminación de las llamas expuestas, las cuales consumen oxígeno del entorno de trabajo. Práctico: se puede colocar en el lugar más cómodo para el operador y está listo para su uso inmediato. Ahorro de energía: consumo mínimo gracias al control de calentamiento con dos sensores externos.',
        imageUrl: 'assets/images/zhermack/50.jpg',
        idModal: 50,
        modalImageUrl: "assets/images/modalImages/zhermack/50.jpg"
      },
      {
        title: "Quasar | Quasar Plus",
        shortDescription: 'Características: Precisa energía térmica que se concentra en el área de la soldadura, Sistema de regulación de la energía que permite establecer la temperatura de calentamiento. Esto permite calentar los elementos que serán soldados de forma gradual y evita que la estructura cristalina sufra daños, Sistema de centrado mecánico para realizar una soldadura por puntos precisa de los elementos. Método de soldadura rápido y fácil de aprender. La soldadura por infrarrojos elimina la combustión y la oxidación durante la fase de calentamiento. El proceso de soldadura Quasar Plus es más rápido que los métodos tradicionales e incluso que el laser.',
        imageUrl: 'assets/images/zhermack/51.jpg',
        idModal: 51,
        modalImageUrl: "assets/images/modalImages/zhermack/51.jpg"
      },
      
  ];
  
  module.exports = {
    zhermack
  };