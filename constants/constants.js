const EMAIL_INFORMATION = {
  FROM: "contacto@coldent.com",
  SUBJECT: "Contacto Pagina Web",
  SEND: "coldentsas@gmail.com",
  TITLE: "Mensaje pagina contacto"
};

const principalProducts = [
  {
    imageUrl: "assets/images/index/1.jpg",
    nombre: "VITA Easyshade® V",
    description: "Determinación óptima del color, seguridad máxima"
  },
  {
    imageUrl: "assets/images/index/2.jpg",
    nombre: "VITA VIONIC® WAX",
    description: "Para el encerado CAD/CAM eficiente"
  },
  {
    imageUrl: "assets/images/index/3.jpg",
    nombre: "VITA VIONIC® BOND",
    description:
      "Para la unión segura de dientes protésicos VITA a la base VITA VIONIC"
  },
  {
    imageUrl: "assets/images/index/7.jpg",
    nombre: "VITAVM®13",
    description:
      "Para el recubrimiento total de estructuras metálicas con un CET convencional"
  },
  {
    imageUrl: "assets/images/index/8.jpg",
    nombre: "VITA VMK Master®",
    description:
      "Para el recubrimiento total de estructuras metálicas con un CET convencional"
  },
  {
    imageUrl: "assets/images/index/9.jpg",
    nombre: "VITA TITANKERAMIK",
    description: "Cerámica de recubrimiento para estructuras de titanio"
  }
];

const menuProducts = [
  { product: "Vita" },
  { product: "Bego" },
  { product: "Ivoclar" },
  { product: "Zhermack" },
  { product: "Yety" },
  { product: "Dentona" },
  /*{ product: "Keystone" },*/
  { product: "WyH" },
  { product: "Renfert" }
];

module.exports = {
  EMAIL_INFORMATION,
  principalProducts,
  menuProducts
};
